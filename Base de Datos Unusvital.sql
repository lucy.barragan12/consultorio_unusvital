create database unusvital_G32;
use unusvital_G32;
Create table Paciente (
DNI_pac varchar (15) not null,
Nombres_pac varchar (50) not null,
Fecha_nacimiento_pac date not null,
telefono_pac varchar (15) not null,
Nombre_EPS varchar (10) not null,
Constraint paciente_pk primary key (DNI_pac));
create table Medico (
codigo_med varchar (10) not null,
Nombres_med varchar (50) not null,
telefono_med varchar (15) not null,
numero_consultorio_med varchar (10) not null,
Fecha_disponilidad_med date not null,
Hora_disponibilidad_med time not null,
constraint medico_pk primary key (codigo_med));
create table Consulta (
Codigo_con varchar (15) not null,
Motivo_con varchar (100) not null,
Diagnostico_con varchar (100) not null,
Tratamiento_con varchar (100) null,
Fecha_con date not null,
Hora_con time null,
DNI_pac varchar (15) not null,
Codigo_med varchar (10) not null,
Constraint consulta_pk primary key (Codigo_con),
Constraint Consulta_DNI_pac_fk foreign key (DNI_pac) references Paciente (DNI_pac),
Constraint Consulta_Código_med_fk foreign key (Codigo_med) references Medico (Codigo_med)); 

Create table Formula (
Numero_for varchar (10) not null,
Medicamento_for varchar (50) not null,
Cantidad_medicamento_for varchar (10) not null,
Fecha_for date not null,
codigo_con varchar (15) not null,
Constraint formula_pk primary key (numero_for),
Constraint formula_codigo_con_fk foreign key (codigo_con) references consulta(codigo_con));

