
package com.consultorio.ConsUnusvital.DAO;

import com.consultorio.ConsUnusvital.Models.AppointmentDoctor;
import org.springframework.data.repository.CrudRepository;
public interface AppointmentDoctorDAO extends CrudRepository<AppointmentDoctor, String>{
    
}
