
package com.consultorio.ConsUnusvital.DAO;

import com.consultorio.ConsUnusvital.Models.Formula;
import org.springframework.data.repository.CrudRepository;
public interface FormulaDAO extends CrudRepository <Formula, String>{
    
}
