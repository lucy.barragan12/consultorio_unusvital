
package com.consultorio.ConsUnusvital.DAO;

import com.consultorio.ConsUnusvital.Models.Registropaciente;
import org.springframework.data.repository.CrudRepository;

public interface RegistropacienteDao extends CrudRepository <Registropaciente, String>{
    
    
}
