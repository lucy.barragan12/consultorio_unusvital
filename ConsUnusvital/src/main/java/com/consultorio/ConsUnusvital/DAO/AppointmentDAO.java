
package com.consultorio.ConsUnusvital.DAO;

import com.consultorio.ConsUnusvital.Models.Appointment;
import org.springframework.data.repository.CrudRepository;
public interface AppointmentDAO extends CrudRepository<Appointment, String>{
    
}
