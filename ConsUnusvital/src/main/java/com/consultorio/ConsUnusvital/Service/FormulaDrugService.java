/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consultorio.ConsUnusvital.Service;
import com.consultorio.ConsUnusvital.Models.FormulaDrug;
import java.util.List;

public interface FormulaDrugService {
     public FormulaDrug save(FormulaDrug formuladrug);
     public void delete (String id);
     public  FormulaDrug findByid(String id);
     public List<FormulaDrug> findByall(); 
    
}
