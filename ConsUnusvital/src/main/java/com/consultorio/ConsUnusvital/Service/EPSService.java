/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consultorio.ConsUnusvital.Service;

import com.consultorio.ConsUnusvital.Models.EPS;
import java.util.List;
public interface EPSService {
    public EPS save(EPS eps);
     public void delete (String id);
     public  EPS findByid(String id);
     public List<EPS> findByall(); 
    
}
