
package com.consultorio.ConsUnusvital.Service;

import com.consultorio.ConsUnusvital.Models.Registropaciente;
import java.util.List;

public interface RegistropacienteService {
    public Registropaciente save(Registropaciente registropaciente);
    public void delete (String id);
    public  Registropaciente findByid(String id);
    public List<Registropaciente> findByall(); 
}
