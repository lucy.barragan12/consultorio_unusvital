/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consultorio.ConsUnusvital.Service;

import com.consultorio.ConsUnusvital.Models.Drug;
import java.util.List;
public interface DrugService {
    
     public Drug save(Drug drug);
     public void delete (String id);
     public  Drug findByid(String id);
     public List<Drug> findByall(); 
}
