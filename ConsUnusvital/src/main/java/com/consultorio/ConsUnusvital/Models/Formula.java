
package com.consultorio.ConsUnusvital.Models;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
@Table
@Entity (name="formula")
public class Formula {
    @Id
    @Column (name="consecutivo_formula")
    private String consecutivo_formula;
    @Column
    private String id_paciente;

    public Formula(String consecutivo_formula, String id_paciente) {
        this.consecutivo_formula = consecutivo_formula;
        this.id_paciente = id_paciente;
    }

    public Formula() {
    }

    public String getConsecutivo_formula() {
        return consecutivo_formula;
    }

    public void setConsecutivo_formula(String consecutivo_formula) {
        this.consecutivo_formula = consecutivo_formula;
    }

    public String getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(String id_paciente) {
        this.id_paciente = id_paciente;
    }
    
    
    
}
