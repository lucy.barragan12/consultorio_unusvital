
package com.consultorio.ConsUnusvital.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Table 
@Entity (name="Drug")
public class Drug {
    @Id
    @Column(name="DrugCode")
    private String DrugCode;
    @Column(name="DrugName")
    private String DrugName;

    public Drug(String DrugCode, String DrugName) {
        this.DrugCode = DrugCode;
        this.DrugName = DrugName;
    }

    public Drug() {
    }

    public String getDrugCode() {
        return DrugCode;
    }

    public void setDrugCode(String DrugCode) {
        this.DrugCode = DrugCode;
    }

    public String getDrugName() {
        return DrugName;
    }

    public void setDrugName(String DrugName) {
        this.DrugName = DrugName;
    }
    
    
    
}
