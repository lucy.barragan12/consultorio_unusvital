
package com.consultorio.ConsUnusvital.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
@Table
@Entity(name="formula_Drug")
public class FormulaDrug {
    @Id
    @Column (name="consecutivo_formula")
    private String consecutivo_formula;
    @Id
    @Column (name="DrugCode")
    private String DrugCode;

    public FormulaDrug(String consecutivo_formula, String DrugCode) {
        this.consecutivo_formula = consecutivo_formula;
        this.DrugCode = DrugCode;
    }

    public FormulaDrug() {
    }

    public String getConsecutivo_formula() {
        return consecutivo_formula;
    }

    public void setConsecutivo_formula(String consecutivo_formula) {
        this.consecutivo_formula = consecutivo_formula;
    }

    public String getDrugCode() {
        return DrugCode;
    }

    public void setDrugCode(String DrugCode) {
        this.DrugCode = DrugCode;
    }
    
    
    
    
}
