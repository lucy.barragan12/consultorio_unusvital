/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.consultorio.ConsUnusvital.Models;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Table
@Entity(name="Registropaciente")
public class Registropaciente implements Serializable {
    @Id
    @Column (name="id_paciente") 
    private String id_paciente; 
    @Column(name="nombre_pac")
    private String nombre_pac ;
    @Column(name="e_mail")
    private String e_mail;
    @Column(name="telefono_pac")
    private String telefono_pac ;

    public Registropaciente(String id_paciente, String nombre_pac, String e_mail, String telefono_pac) {
        this.id_paciente = id_paciente;
        this.nombre_pac = nombre_pac;
        this.e_mail = e_mail;
        this.telefono_pac = telefono_pac;
    }

    public Registropaciente() {
    }

    public String getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(String id_paciente) {
        this.id_paciente = id_paciente;
    }

    public String getNombre_pac() {
        return nombre_pac;
    }

    public void setNombre_pac(String nombre_pac) {
        this.nombre_pac = nombre_pac;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public String getTelefono_pac() {
        return telefono_pac;
    }

    public void setTelefono_pac(String telefono_pac) {
        this.telefono_pac = telefono_pac;
    }
    
    
}
