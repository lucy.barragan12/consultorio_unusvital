
package com.consultorio.ConsUnusvital.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Table
@Entity(name="appointment")
public class Appointment {
    @Id
    @Column (name="AppointmentCode")
    private String AppointmentCode;
    @Column (name="DoctorCode")
    private String DoctorCode;
    @Column (name="NumberOffice")
    private String NumberOffice;
    @Column (name="AppointmentDate")
    private String AppointmentDate;
    @Column (name="Appointmenthour")
    private String Appointmenthour;

    public Appointment(String AppointmentCode, String DoctorCode, String NumberOffice, String AppointmentDate, String Appointmenthour) {
        this.AppointmentCode = AppointmentCode;
        this.DoctorCode = DoctorCode;
        this.NumberOffice = NumberOffice;
        this.AppointmentDate = AppointmentDate;
        this.Appointmenthour = Appointmenthour;
    }

    public Appointment() {
    }

    public String getAppointmentCode() {
        return AppointmentCode;
    }

    public void setAppointmentCode(String AppointmentCode) {
        this.AppointmentCode = AppointmentCode;
    }

    public String getDoctorCode() {
        return DoctorCode;
    }

    public void setDoctorCode(String DoctorCode) {
        this.DoctorCode = DoctorCode;
    }

    public String getNumberOffice() {
        return NumberOffice;
    }

    public void setNumberOffice(String NumberOffice) {
        this.NumberOffice = NumberOffice;
    }

    public String getAppointmentDate() {
        return AppointmentDate;
    }

    public void setAppointmentDate(String AppointmentDate) {
        this.AppointmentDate = AppointmentDate;
    }

    public String getAppointmenthour() {
        return Appointmenthour;
    }

    public void setAppointmenthour(String Appointmenthour) {
        this.Appointmenthour = Appointmenthour;
    }
    
    
    
}
