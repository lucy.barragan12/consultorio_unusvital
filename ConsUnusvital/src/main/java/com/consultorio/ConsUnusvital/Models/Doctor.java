
package com.consultorio.ConsUnusvital.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Table
@Entity (name="Doctor")
public class Doctor {
    @Id
    @Column (name="DoctorCode")
    private String DoctorCode;
    @Column (name="DoctorName")
    private String DoctorName;
    @Column (name="email")
    private String email;
    @Column (name="TelephonNumber")
    private String TelephonNumber;

    public Doctor(String DoctorCode, String DoctorName, String email, String TelephonNumber) {
        this.DoctorCode = DoctorCode;
        this.DoctorName = DoctorName;
        this.email = email;
        this.TelephonNumber = TelephonNumber;
    }

    public Doctor() {
    }

    public String getDoctorCode() {
        return DoctorCode;
    }

    public void setDoctorCode(String DoctorCode) {
        this.DoctorCode = DoctorCode;
    }

    public String getDoctorName() {
        return DoctorName;
    }

    public void setDoctorName(String DoctorName) {
        this.DoctorName = DoctorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephonNumber() {
        return TelephonNumber;
    }

    public void setTelephonNumber(String TelephonNumber) {
        this.TelephonNumber = TelephonNumber;
    }
    
    
    
}
