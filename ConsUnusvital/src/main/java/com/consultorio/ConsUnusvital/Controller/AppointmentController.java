package com.consultorio.ConsUnusvital.Controller;

import com.consultorio.ConsUnusvital.Models.Appointment;
import com.consultorio.ConsUnusvital.Service.AppointmentService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/Appointment")
public class AppointmentController {
    @Autowired
    private AppointmentService AppointmentService;
    
    @PostMapping(value="/")
    public ResponseEntity <Appointment> agregar(@RequestBody Appointment appointment){
        Appointment obj=AppointmentService.save(appointment);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Appointment> eliminar(@PathVariable String id){
        Appointment obj = AppointmentService.findByid(id);
        if (obj!=null)
            AppointmentService.delete(id);
        else
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value="/")
    public ResponseEntity<Appointment> editar(@RequestBody Appointment appointment){
        Appointment obj = AppointmentService.findByid(appointment.getDoctorCode());
        if(obj!=null){
            obj.setDoctorCode(appointment.getDoctorCode());
           
            AppointmentService.save(obj);
            
            
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @GetMapping("/List")
    public List<Appointment> consultarTodo(){
        return AppointmentService.findByall();
    }
    @GetMapping("/List/{id}")
    public Appointment consultaPorId(@PathVariable String id){
        return AppointmentService.findByid(id);
    }
 
}
