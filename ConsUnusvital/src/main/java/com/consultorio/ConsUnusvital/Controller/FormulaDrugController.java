
package com.consultorio.ConsUnusvital.Controller;

import com.consultorio.ConsUnusvital.Models.FormulaDrug;
import com.consultorio.ConsUnusvital.Service.FormulaDrugService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/FormulaDrug")
public class FormulaDrugController {
    @Autowired
    private FormulaDrugService FormulaDrugService;
    
    @PostMapping(value="/")
    public ResponseEntity <FormulaDrug> agregar(@RequestBody FormulaDrug formuladrug){
        FormulaDrug obj=FormulaDrugService.save(formuladrug);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @DeleteMapping(value="/{id}")
    public ResponseEntity<FormulaDrug> eliminar(@PathVariable String id){
        FormulaDrug obj = FormulaDrugService.findByid(id);
        if (obj!=null)
            FormulaDrugService.delete(id);
        else
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value="/")
    public ResponseEntity<FormulaDrug> editar(@RequestBody FormulaDrug formuladrug){
        FormulaDrug obj = FormulaDrugService.findByid(formuladrug.getConsecutivo_formula());
        if(obj!=null){
            obj.setDrugCode(formuladrug.getDrugCode());
           
            FormulaDrugService.save(obj);
            
            
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @GetMapping("/List")
    public List<FormulaDrug> consultarTodo(){
        return FormulaDrugService.findByall();
    }
    @GetMapping("/List/{id}")
    public FormulaDrug consultaPorId(@PathVariable String id){
        return FormulaDrugService.findByid(id);
    }
 
}
