package com.consultorio.ConsUnusvital.Controller;

import com.consultorio.ConsUnusvital.Models.Drug;
import com.consultorio.ConsUnusvital.Service.DrugService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/Drug")
public class DrugController {
    @Autowired
    private DrugService DrugService;
    
    @PostMapping(value="/")
    public ResponseEntity <Drug> agregar(@RequestBody Drug drug){
        Drug obj=DrugService.save(drug);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Drug> eliminar(@PathVariable String id){
        Drug obj = DrugService.findByid(id);
        if (obj!=null)
            DrugService.delete(id);
        else
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
   
    @PutMapping(value="/")
    public ResponseEntity<Drug> editar(@RequestBody Drug drug){
        Drug obj = DrugService.findByid(drug.getDrugCode());
        if(obj!=null){
            obj.setDrugName(drug.getDrugName());
           
           DrugService.save(obj);
            
            
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        
    }
    @GetMapping("/List")
    public List<Drug> consultarTodo(){
        return DrugService.findByall();
    }
    @GetMapping("/List/{id}")
    public Drug consultaPorId(@PathVariable String id){
        return DrugService.findByid(id);
    }
 
}
